Name:      lscsoft-backports-testing-debug-config
Version:   7
Release:   1%{?dist}
Summary:   Repository configuration for LSCSoft Backports Testing Debug

Group:     System Environment/Base
License:   GPLv3+
Source0:   lscsoft-backports-testing-debug.repo
BuildArch: noarch
Requires:  sl-release >= %{version}

%description
Repository configuration for LSCSoft Backports Testing Debug

%prep
%setup -q -c -T

%build

%install
rm -rf $RPM_BUILD_ROOT
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0,root,root,-)
%config /etc/yum.repos.d/*

# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Fri Apr 03 2020 Adam Mercer <adam.mercer@ligo.org> 7-1
- initial release
